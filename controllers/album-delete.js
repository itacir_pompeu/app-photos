// file: controllers/album-delete.js - created at 2016-02-10, 03:16
'use strict';
const Albuns = require('../models/albuns');

function albumDeleteHandler(req, res) {
  Albuns.remove({_id : req.params.id})
    .then(result => {
      res.status(204).json(result.result);
    })
    .catch(err => {
      res.status(404).send({err : err});
    });
}

module.exports = albumDeleteHandler;
