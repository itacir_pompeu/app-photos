// file: controllers/photos-get.js - created at 2016-02-18, 06:55
'use strict';

const Photo =  require('../models/index').Photos;
// todo get fron drop box
const Dropbox = require('dropbox-api-v2');

function photosGetHandler(req, res) {
  /*
const dropbox =  new Dropbox();
const options = {
folder : '',
isRecursive : true,
token : token
};
*/
  const album = new RegExp(`^${req.params.album}$`, 'i');
  Photo.find({ client: album })
    .then(photo => {
      res.json(photo);
        /* options.folder = `/${photo.images[0].split('/')[3]}`;
      dropbox.getFolders(options)
      .then(data => res.json(JSON.parse(data).entries))
      .catch(err => res.status(400).end(err));*/
    })
    .catch(err => { 
      res.status(400).json({ err : err});
    });

}
module.exports = exports = photosGetHandler;
