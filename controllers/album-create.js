// file: controllers/album-create.js - created at 2016-02-10, 03:15
'use strict';

const Album = require('../models/albuns');

function albumCreateHandler(req, res) {

  Album.create(req.body)
    .then(album => {
      res.status(201).json(album);
    })
    .catch(err => {
      res.status(404).json({err : err});
    });
}

module.exports = exports = albumCreateHandler;
