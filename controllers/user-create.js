// file: controllers/user.js - created at 2016-02-08, 07:02
'use strict';

const User = require('../models/user');

function userCreate(req, res) {
  User.create(req.body)
  .then(user => res.status(201).json(user))
  .catch(err => res.status(404).json({err : err}));
}

module.exports = userCreate;
