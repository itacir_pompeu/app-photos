//file: controllers/photos-create.js - created at 2016-02-16, 11:02

'use strict';

const Photo      = require('../models/index').Photos;
const formidable = require('formidable');
const fs         = require('fs');
const R          = require('ramda');

function photosCreateHandler(req, res) {
  let form   = new formidable.IncomingForm();
  let fields = [];
  let files  = [];
  let path   = '';

  form.parse(req);

  form
    .on('field', (field, value) => {
      fields.push([field, value]);
      if (field === 'client') {
        path = madeClientDir(value);
      }
      if (field === 'event') {
        form.uploadDir = isPathValid(path,value);
      }
    })
    .on('file', (field, file) => {
      if (!/png|jpeg/.test(file.type)) {
        fs.unlink(file.path);
        form.removeAllListeners();
        return res.status(400).json({err : 'iccept one png or jpg files'});
      }

      let newPath = `${form.uploadDir}/${file.name.toLowerCase()}`;
      renameImage(file.path, newPath);
      files.push(newPath.replace(/\.\./,''));
    })
    .on('end', () => {
      fields.push(['images' , files]);
      fields = R.fromPairs(fields);
      Photo.create(fields)
        .then(photo => {
          return res.status(201).json(photo);
        },err => {
          return res.status(400).json(err);
        });
    });
}

module.exports = exports = photosCreateHandler;

function madeClientDir(value) {
  value = `./public/images/${value}`;
  return value.split(' ')
    .map(f => f+='_')
    .toString()
    .replace(/,/g,'')
    .toLowerCase();
}

function isPathValid (path,value) {
  path = `${path}${value.toLowerCase()}`;
  try {
    fs.accessSync(path, fs.R_OK);
  } catch (e) {
    fs.mkdirSync(path);
  }
  return path;
}

function renameImage(path,newPath) {
  fs.exists(path, exist => {
    if (exist) {
      fs.rename(path, newPath);
    }
  });
}
