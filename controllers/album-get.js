// file: controllers/album-get.js - created at 2016-02-10, 03:15
'use strict';
const Albuns = require('../models/albuns');

function albumGetHandler(req, res) {
  Albuns.find({})
  .then(albuns => {
    res.json(albuns);
  })
  .catch(err => {
    res.status(404).json({ err : err});
  });
}
module.exports = exports = albumGetHandler;
