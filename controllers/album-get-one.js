// file: controllers/album-get-one.js - created at 2016-02-10, 03:15
'use strict';

const Albuns = require('../models/albuns');

function albumGetOneHandler(req, res) {
  Albuns.findOne({_id : req.params.id})
  .then(user => {
    res.json(user);
  })
  .catch(err => {
    res.status(404).send({err : err});
  });
}
module.exports = albumGetOneHandler;
