// file: controllers/auth.js - created at 2016-02-20, 03:15
'use strict';

const jwt       = require('jsonwebtoken');
const bcrypt    = require('bcrypt');
const getSecret = require('../configs/index').apiKey;
const User      = require('../models/index').User;

function authHandler (req, res) {
  const { email, password } = req.body;

  User.findOne({email : email})
    .then(user => comparePasswords(user, password))
    .then(createToken)
    .then(token => sendToken(token, res))
    .catch(() => notAthorized(res));
}

module.exports = authHandler;

function sendToken (token, res) {
  res.set('Authorization', `Bearer ${token}`);
  res.json({ token });
}

function notAthorized (res) {
  res.set('Authorization', 'not_authorized');
  res.status(401).json({ authorization : 'not_authorized' });
}

function comparePasswords (user, password) {
  if(bcrypt.compareSync(password, user.password)){
    return Promise.resolve(user);
  }
  return Promise.reject('invalid user od password');
}

function createToken(user) {
  let objUser  = user.toObject();
  delete objUser.password;
  return Promise.resolve(jwt.sign(objUser, getSecret.key , { expiresIn : 60 * 5 }));
}
