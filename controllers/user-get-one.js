// file: controllers/user-get-one.js - created at 2016-02-08, 10:08
'use strict';

const User = require('../models/user');

function userGetOneHandler(req, res) {

  User.findOne({_id : req.params.id})
  .then(user => {
    res.json(user);
  })
  .catch(err => {
    res.status(404).json({err : err});
  });
}

module.exports = exports = userGetOneHandler;
