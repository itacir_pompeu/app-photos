// file: controllers/album-update.js - created at 2016-02-10, 03:16
'use strict';

const Album = require('../models/albuns');

function albumUpdateHandler(req, res) {
  Album.findOneAndUpdate({_id : req.params.id}, req.body)
  .then(updated => {
    res.status(202).json(updated);
  })
  .catch(err => {
    res.status(404).json({err : err});
  });
}

module.exports = albumUpdateHandler;
