// file: controllers/user-delete.js - created at 2016-02-08, 09:27
'use strict';
const User = require('../models/user');

function userDeleteHandler(req, res) {
  User.remove({_id : req.params.id})
  .then(deleted => {
    res.status(204).json(deleted.result);
  })
  .catch(err => {
    res.json({err : err});
  }); 
}
module.exports = exports = userDeleteHandler;
