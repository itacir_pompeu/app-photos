// file: controllers/user-update.js - created at 2016-02-08, 10:12
'use strict';

const User = require('../models/user');

function userUpdateHandler(req, res) {
  User.findOneAndUpdate({_id : req.params.id}, req.body)
  .then(updated => {
    res.status(202).json(updated);
  })
  .catch(err => {
    res.status(404).send({err : err});
  });
}
module.exports = exports = userUpdateHandler;
