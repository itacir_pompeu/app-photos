// file: controllers/index.js
'use strict';

exports.userCreate = require('./user-create');
exports.userGet    = require('./user-get');
exports.userDelete = require('./user-delete');
exports.userGetOne = require('./user-get-one');
exports.userUpdate = require('./user-update');

exports.albumGet    = require('./album-get');
exports.albumCreate = require('./album-create');
exports.albumGetOne = require('./album-get-one');
exports.albumUpdate = require('./album-update');
exports.albumDelete = require('./album-delete');

exports.photosCreate = require('./photos-create');
exports.photosGet = require('./photos-get');
exports.auth = require('./auth');
