// file: controllers/user-get.js - created at 2016-02-08, 08:49
'use strict';

const User = require('../models/user');

function userGetHandler(req, res) {
  User.find({})
  .then(users => {
    res.json(users);
  })
  .catch(err => {
    res.status(404).json({err : err});
  });
}

module.exports = userGetHandler;
