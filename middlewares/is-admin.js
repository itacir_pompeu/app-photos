// file: middlewares/is-admin.js - created at 2016-03-01, 07:17
'use strict';

const jwt       = require('jsonwebtoken');
const getSecret = require('../configs/index').apiKey;

function isAdminHandler(req, res, next) {
  const token  = req.headers.authorization.split(' ')[1];
  let userAuth = {};
  try {
    userAuth  = jwt.verify(token, getSecret.key);
  } catch (e) {
    return res.status(401).json({ authorization : e.message});
  }

  if (userAuth.nivel !== 'CLIENT') {
    return next();
  }

  res.set('Authorization', 'not_authorized');
  res.status(401).json({ authorization : 'not_authorized' });

}

module.exports = isAdminHandler;
