'use strict';

const path         = require('path');
const express      = require('express');
const jwt          = require('express-jwt');
const secret       = require('./configs/index').apiKey.key;
//todo verify auth
const logger       = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser   = require('body-parser');
const dbURI        = require('./configs').db;
const mongoose     = require('mongoose');

mongoose.connect(dbURI);

const routes = require('./routes/index');
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(jwt({ secret }).unless({
  path: ['/', '/api/v1/auth', { url :'/api/v1/users', methods : 'POST'}]
}));

app.use('/', routes.site);
app.use('/api/v1/photos', routes.photos);
app.use('/api/v1/auth'  , routes.auth);
app.use('/api/v1/users' , routes.users);
app.use('/api/v1/albuns', routes.albuns);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
