// file: tests/upload-photos.test.js - created at 2016-02-16, 08:37
'use strict';

const http       = require('http');
const Photo      = require('../models/photos');
const app        = require('../app');
const request    = require('supertest');
const pathPhotos = '/api/v1/photos';
const UserBuilder = require('./data-builders/user.builder');

describe('upload-photos', function () {
  this.timeout(10000);
  let server = null;
  let headerToken = '';

  before(done => {
    ch.setLocale('pt-BR');
    server = http.createServer(app);
    server.listen(3000);
    UserBuilder.createAdminToken()
      .then(token => {
        headerToken = token;
        done();
      });
  });

  afterEach(done => {
    Photo.remove({})
      .then(() => done())
      .catch(done);
  });

  after(done => {
    setTimeout(()=> {
      done();
    }, 4000);
  });

  describe('upload-photos create', () => {

    it('upload photos accept only .jpg|.png', done => {
      request(app)
        .post(pathPhotos)
        .field('client','Itacir Ferreira Pompeu')
        .field('event','formatura')
        .attach('image',__dirname+'/images/invalid.txt')
        .set('Authorization', headerToken)
        .set('Accept', 'application/json')
        .expect('Content-Type',/json/)
        .expect(400)
        .end((err, result) => {
          expect(err).to.not.be.exist;
          expect(result).to.be.exist;
          expect(result.body.err).to.be.eql('iccept one png or jpg files');
          done();
        });
    });

    it('upload photos *.png', done => {
      request(app)
        .post(pathPhotos)
        .field('client','Itacir Ferreira Pompeu')
        .field('event','formatura')
        .attach('image',__dirname+'/images/apple_ex.png')
        .set('Accept', 'application/json')
        .expect('Content-Type',/json/)
        .set('Authorization', headerToken)
        .expect(201)
        .end((err, result) => {
          expect(err).to.not.be.exist;
          expect(result).to.be.exist;
          expect(result.body.err).to.not.be.exit;
          done();
        });
    });

    it('upload photos *.jpg', done => {
      request(app)
        .post(pathPhotos)
        .field('client','Itacir Ferreira Pompeu')
        .field('event','formatura')
        .attach('image',__dirname+'/images/mini.jpg')
        .set('Accept', 'application/json')
        .set('Authorization', headerToken)
        .expect('Content-Type',/json/)
        .expect(201)
        .end((err, result) => {
          expect(err).to.not.be.exist;
          expect(result).to.be.exist;
          expect(result.body.err).to.not.be.exit;
          done();
        });
    });

    it('upload two photos *.jpg and *.png', done => {
      const body =  { 
        client: 'Itacir Ferreira Pompeu',
        event: 'formatura',
        images: 
          [ '/public/images/itacir_ferreira_pompeu_formatura/mini.jpg',
            '/public/images/itacir_ferreira_pompeu_formatura/apple_ex.png' ]
      };

      request(app)
        .post(pathPhotos)
        .field('client','Itacir Ferreira Pompeu')
        .field('event','formatura')
        .attach('image',__dirname+'/images/mini.jpg')
        .attach('image',__dirname+'/images/apple_ex.png')
        .set('Authorization', headerToken)
        .set('Accept', 'application/json')
        .expect('Content-Type',/json/)
        .expect(201)
        .end((err, result) => {
          expect(err).to.not.be.exist;
          expect(result).to.be.exist;
          expect(result.body.err).to.not.be.exit;
          expect(result.body.client).to.be.eq(body.client);
          expect(result.body.event).to.be.eq(body.event);
          expect(result.body.images).to.be.length(2);
          done();
        });
    });

  });
});
