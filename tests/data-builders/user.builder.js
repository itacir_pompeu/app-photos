'use strict';

const User = require('../../models/user');
//const nivels = ['ADMIN', 'DEV', 'CLIENT'];
const jwt        = require('jsonwebtoken');
const getSecret  = require('../../configs/index').apiKey;

function createAdmin () {
  const user = {
    name : ch.Name.name(),
    email : ch.Internet.email(),
    password : ch.Internet.password(8,16),
    nivel  : 'ADMIN'
  };
  return User.create(user);
}

function createAdminToken () {
  return createAdmin()
    .then(user => {
      let objUser = user.toObject();
      delete objUser.password;
      let headerToken = 'Bearer ';
      headerToken +=  jwt.sign(objUser, getSecret.key, { expiresIn: 60*5});
      return headerToken;
    });
}

module.exports = {
  createAdminToken
};
