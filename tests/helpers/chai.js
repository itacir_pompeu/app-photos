'use strict';

const chai = require('chai');
const ch   = require('charlatan');

chai.config.includeStack = true;

global.expect = chai.expect;
global.AssertionError = chai.AssertionError;
global.Assertion = chai.Assertion;
global.assert = chai.assert;
global.ch = ch;
