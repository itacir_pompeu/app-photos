// file: tests/user-model.test.js - created at 2016-02-10, 04:09
'use strict';


describe('user-model', function () {

  const User   = require('../models/user');
  const nivels = ['ADMIN', 'DEV', 'CLIENT'];

  const user = {
    name : ch.Name.name(),
    email : ch.Internet.email(),
    password : ch.Internet.password(8,16),
    nivel  : nivels[ch.Number.between(0,2)]
  };

  describe('user-model password has be encripted', () => {
    it('passworld encripted by bcrypt', () => {
      let u = new User(user);
      expect(u.password).to.have.length(60);
    });
  });

  describe('user-model admin only can add users', () => {

    it('only admin can save new users', done => {
      let newU = {
        name : ch.Name.name(),
        email : ch.Internet.email(),
        password : ch.Internet.password(8,16),
        nivel  : nivels[2]
      };
      let resUser = new User(newU);
      resUser.save()
        .then(u => {
          expect(u).to.not.exist;
          done();
        })
        .catch(err => {
          expect(err).to.be.an.instanceof(Error);
          expect(err).to.match(/^Error: only admin can save new users$/g);
          done();
        });
    });
  });

});
