// file: tests/get-photos.test.js - created at 2016-02-18, 06:22
'use strict';

const http       = require('http');
const app        = require('../app');
const request    = require('supertest');
const pathPhotos = '/api/v1/photos';
const UserBuilder = require('./data-builders/user.builder');
const User = require('../models/user');

describe('getPhotos', function () {
  this.timeout(10000);
  let server = null;
  let headerToken = '';

  before( done => {
    ch.setLocale('pt-BR');
    server = http.createServer(app);
    server.listen(3000);
    UserBuilder.createAdminToken()
      .then(token => {
        headerToken = token;
        done();
      });
  });

  after(done => {
    server.close();
    User.remove({}, () => done());
  });

  describe('upload-photos getall by colection name', () => {
    it('upload photos use route /api/v1/photos', done => {

      request(app)
        .get(pathPhotos+'/itacir')
        .set('Authorization', headerToken)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          expect(err).to.not.exist;
          expect(res.body).to.be.an('array');
          done();
        });

    });
  });

});
