// file: tests/albuns.test.js - created at 2016-02-08, 10:29
'use strict';

const http        = require('http');
const app         = require('../app');
const request     = require('supertest');
const pathAlbuns  = '/api/v1/albuns';
const Albun       = require('../models/albuns');
const UserBuilder = require('./data-builders/user.builder');
const User        = require('../models/user');

describe('Albuns modules', () => {
  let server = null;
  let id = null;
  let headerToken = '';

  const album = {
    event : ch.Name.name(),
    urls  : [ch.Internet.domainName(),ch.Internet.domainName()]
  };

  before(done => {
    ch.setLocale('pt-BR');
    server = http.createServer(app);
    server.listen(3000);
    UserBuilder.createAdminToken()
      .then(token => {
        headerToken = token;
        done();
      });
  });

  after(done => {
    Albun.remove({});
    User.remove({});
    server.close();
    done();
  });

  describe('create a album in db', () => {
    it('expect an album created', done => {
      request(app)
        .post(pathAlbuns)
        .set('Authorization', headerToken)
        .send(album)
        .expect('Content-Type', /json/)
        .expect(201)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.exist;
          id = res.body._id;
          expect(res.body._id).to.be.exist;
          expect(res.body.url).to.be.equal(album.url);
          done();
        });
    });
  });

  describe('update a album in db', () => {
    it('expect an album updated', done => {

      const newAlbum = {
        url : ch.Internet.domainWord()
      };

      request(app)
        .put(`${pathAlbuns}/${id}`)
        .set('Authorization', headerToken)
        .send(newAlbum)
        .expect('Content-Type', /json/)
        .expect(202)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.exist;
          done();
        });
    });
  });

  describe('get album by id', () => {
    it('expect get an album by id', done => {
      request(app)
        .get(`${pathAlbuns}/${id}`)
        .set('Authorization', headerToken)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err,res) => {
          expect(err).to.be.null;
          expect(res).to.be.exist;
          expect(res.body).to.be.a('object');
          done();
        });
    });
  });

  describe('expect get all albums from db', () => {
    it('get all album from db', done => {
      request(app)
        .get(pathAlbuns)
        .set('Authorization', headerToken)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.exist;
          expect(res.body).to.be.a('array');
          done();
        });
    });
  });

  describe('delete album from db', () => {
    it('expect remove album by id', done => {
      request(app)
        .del(`${pathAlbuns}/${id}`)
        .set('Authorization', headerToken)
        .expect(204)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.exist;
          done();
        });
    });
  });
});
