// file: tests/auth.test.js - created at 2016-02-20, 01:35

'use strict';

const http       = require('http');
const app        = require('../app');
const request    = require('supertest');
const pathAuth   = '/api/v1/auth';
const User       = require('../models/user');
const jwt        = require('jsonwebtoken');
const getSecret  = require('../configs/index').apiKey;


describe('auth jwt', () => {
  let server = {};
  let id ='';
  let user = {
    name : ch.Name.name(),
    email : ch.Internet.email(),
    password : ch.Internet.password(8,16),
    nivel  : 'DEV'
  };

  function testHelper (body, cb) {
    request(app)
      .post(pathAuth)
      .send(body)
      .expect('Content-Type', /json/)
      .end(cb);
  }

  before('before all', done => {
    server = http.createServer(app);
    server.listen(3000);
    User.create(user).then(u => {
      id = u._id;
      done();
    });
  });

  after('after all', done => {
    server.close();
    User.remove({_id : id}, done);
  });

  describe('auth retrive token', () => {
    it('expect recive token valid if user exist', done => {
      function endHandler (err, res) {
        const secret = getSecret.key;
        const token = res.body.token;
        expect(err).to.be.null;
        expect(res).to.be.exist;
        expect(res.header.authorization).to.be.exist;
        expect(res.status).to.eql(200);
        jwt.verify(token, secret, (err , decoded) => {
          expect(decoded).to.not.have.property('password');
          done();
        });
      }

      testHelper(user,endHandler); 
    });

    it('expect error if email or password is not ok', done => {

      const login = {
        email : user.email,
        password : '1231231adf'
      };

      function endHandler(err, res) {
        expect(err).to.be.null;
        expect(res).to.exist;
        expect(res.header.authorization).to.eql('not_authorized');
        expect(res.body.authorization).to.eql('not_authorized');
        expect(res.status).to.eql(401);
        done();
      }

      testHelper(login,endHandler); 
    });
  });
});

