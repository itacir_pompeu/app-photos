'use strict';

const http      = require('http');
const app       = require('../app');
const request   = require('supertest');
const User      = require('../models/user');
const pathUsers = '/api/v1/users';

describe('User spect crud', () => {
  let server = null;
  let id = null;
  let headerToken = 'Bearer ';
  const nivels = ['ADMIN', 'DEV', 'CLIENT'];

  const user = {
    name : ch.Name.name(),
    email : ch.Internet.email(),
    password : ch.Internet.password(8,16),
    nivel  : nivels[ch.Number.between(0,1)]
  };

  before( done => {
    ch.setLocale('pt-BR');
    server = http.createServer(app);
    server.listen(3000);
    User.create(user).then(() => {
      request(app)
        .post('/api/v1/auth')
        .send(user)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          headerToken += res.body.token;
          done();
        });
    });
  });

  after(done => {
    server.close();
    User.remove({email : user.email},done);
  });

  describe('expect get all users from db', () => {
    it('get all user from db', done => {
      request(app)
        .get(pathUsers)
        .set('Authorization', headerToken)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.exist;
          expect(res.body).to.be.a('array');
          done();
        });
    });
  });

  describe('create a user in db', () => {
    it('expect an user created', done => {
      request(app)
        .post(pathUsers)
        .send(user)
        .expect('Content-Type', /json/)
        .expect(201)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.exist;
          id = res.body._id;
          expect(res.body._id).to.be.exist;
          expect(res.body.name).to.be.equal(user.name);
          expect(res.body.email).to.be.equal(user.email);
          expect(res.body.password).not.to.be.equal(user.password);
          done();
        });
    });
  });

  describe('update a user in db', () => {
    it('expect an user updated', done => {

      const newUser = {
        name : ch.Name.name(),
        email : ch.Internet.email(),
        password : ch.Internet.password(8,16)
      };

      request(app)
        .put(`${pathUsers}/${id}`)
        .send(newUser)
        .set('Authorization', headerToken)
        .expect('Content-Type', /json/)
        .expect(202)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.exist;
          done();
        });
    });
  });

  describe('get user by id', () => {
    it('expect get an user by id', done => {
      request(app)
        .get(`${pathUsers}/${id}`)
        .set('Authorization', headerToken)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err,res) => {
          expect(err).to.be.null;
          expect(res).to.be.exist;
          expect(res.body).to.be.a('object');
          done();
        });
    });
  });

  describe('delete user from db', () => {
    it('expect remove user by id', done => {
      request(app)
        .del(`${pathUsers}/${id}`)
        .set('Authorization', headerToken)
        .expect(204)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.exist;
          done();
        });
    });
  });

});
