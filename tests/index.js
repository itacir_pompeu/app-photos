// file: tests/index.js
'use strict';

exports.user = require('./user.test');
exports.albuns = require('./albuns.test');
exports.userModel = require('./user-model.test');
exports.uploadPhotos = require('./upload-photos.test');
exports.getPhotos = require('./get-photos.test');
exports.auth = require('./auth.test');
