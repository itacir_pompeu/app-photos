'use strict';

const isAdmin = require('../../middlewares/index').isAdmin;

describe('is-admin', () => {
  it('expect middleware is-admin show status of jwt', () => {
    const req  = {
      headers :  {
        authorization :'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NmQ2MDdiZTNkNjM1NzM2NTY5ZjA4OWEiLCJuYW1lIjoicG9tcGV1IiwiZW1haWwiOiJpdGFjaXJAaG90bWFpbC5jb20iLCJuaXZlbCI6IkRFViIsIl9fdiI6MCwiaWF0IjoxNDU2ODY5NzU5LCJleHAiOjE0NTY4NzAwNTl9.iklum044_2GxiKi4nfZpvoccyUXMy3IvC8ZlSd0RcCk'
      }
    };

    const res = {
      status : function (status) {
        if(status === 401) {
          return this;
        }
      },
      json : function (res) {
        expect(res).to.be.eql({ authorization: 'jwt expired'});
      }
    };

    isAdmin(req,res);

  });
});
