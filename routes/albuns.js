// file: routes/albuns.js - created at 2016-02-10, 03:13
'use strict';

const express = require('express');
const router  = express.Router();
const ctrl    = require('../controllers/index');

router
  .get('/', ctrl.albumGet)
  .post('/',ctrl.albumCreate)
  .get('/:id', ctrl.albumGetOne)
  .put('/:id', ctrl.albumUpdate)
  .delete('/:id',ctrl.albumDelete);

module.exports = router;
