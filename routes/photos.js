// file: routes/photos.js - created at 2016-02-16, 10:59
'use strict';

const express = require('express');
const router  = express.Router();
const ctrl    = require('../controllers/index');

router
  .post('/', ctrl.photosCreate)
  .get('/:album', ctrl.photosGet);


module.exports = router;
