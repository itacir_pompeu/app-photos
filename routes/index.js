'use strict';

exports.site = require('./site');
exports.users = require('./users');
exports.albuns = require('./albuns');
exports.photos = require('./photos');
exports.auth = require('./auth');
