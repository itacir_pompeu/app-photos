// file: routes/albuns.js - created at 2016-02-10, 03:13
'use strict';

const express = require('express');
const router = express.Router();

router
.get('/', (req,res) => {
  res.render('index');
});

module.exports = router;
