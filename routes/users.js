'use strict';

const express  = require('express');
const router   = express.Router();
const ctrl     = require('../controllers/index');
/* GET users listing. */
router
.get('/', ctrl.userGet)
.post('/', ctrl.userCreate)
.get('/:id', ctrl.userGetOne)
.put('/:id', ctrl.userUpdate)
.delete('/:id', ctrl.userDelete);

module.exports = router;
