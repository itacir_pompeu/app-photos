// file: routes/auth.js - created at 2016-02-20, 03:14
'use strict';

const express = require('express');
const router = express.Router();
const ctrl   = require('../controllers/index');

router
.post('/',ctrl.auth);


module.exports = router;
