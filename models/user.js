// file: models/user.js - created at 2016-02-08, 07:02
'use strict';

const mongoose = require('mongoose');
const Schema   = mongoose.Schema;
const bcrypt   = require('bcrypt');

const schema = new Schema({
  name     : {type : String, required : true, trin : true},
  email    : {type : String, required : true, trin : true},
  password : {type : String, required : true, set : passwordEncripted},
  nivel    : {type : String, required : true, enum: ['ADMIN', 'DEV', 'CLIENT']}
});

schema.pre('save', function(next){
  if (this.nivel === 'CLIENT') {
    const err = new Error('only admin can save new users');
    return next(err);
  }
  next();
});

module.exports = mongoose.model('User', schema);

function passwordEncripted (pass) {
  const salt = bcrypt.genSaltSync(8);
  const hash = bcrypt.hashSync(pass, salt);
  return hash;
}

