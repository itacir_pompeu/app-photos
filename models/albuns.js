// file: models/albuns.js - created at 2016-02-08, 10:29
'use strict';

const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const schema = new Schema({
  event      : {type  : String, required : true, trin  : true},
  urls       : [{type : String, required : true, trin  : true}],
  _woner     : {type  : Schema.ObjectId , ref : 'User'},
  //todo experations para indiponibilidade
  created_at : {type  : Date, default : Date.now },
  update_at  : {type  : Date }
});

module.exports =  mongoose.model('Albuns', schema);

