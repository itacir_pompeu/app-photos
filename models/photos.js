// file: models/photos.js - created at 2016-02-18, 03:08
'use strict';

const util     = require('util');
const fs       = require('fs');
const Dropbox  = require('dropbox-api-v2');
const token    = require('../configs/index').token;
const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const schema = new Schema({
  client : {type  : String, required : true, unique : true},
  event  : {type  : String, required  : true},
  images : [{type : String, required : true}]
});

schema.pre('save',true, function(next, done) {
  let dropbox =  new Dropbox();
  let folder = this.images.map(url => `/${url.split('/')[3]}`)[0];
  const options = {
    folder : folder,
    token  : token
  };

  dropbox.createFolder(options)
  .then(data => {
    util.log(data);
    schema.emit('sendfile',this);
    done();
  })
  .catch(err => {
    util.log(err);
    done();
  });
  next();
});

schema.on('sendfile', a =>{
  const dropbox =  new Dropbox();
  const folder = `/${a.images[0].split('/')[3]}`;

  let options = {
    folder : folder ,
    token : token,
    buffer : '',
    fileName : '' 
  };

  a.images.forEach(f => {
    fs.readFile(f, (err,data) => {
      options.buffer = data;
      options.fileName = f.split('/')[4];
      dropbox.upload(options)
      .then(data => {
        util.log(data);
      });
    });
  });
});

module.exports =  mongoose.model('Photos', schema);

