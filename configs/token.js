// file: configs/token.js - created at 2016-02-18, 03:47
'use strict';

function tokenHandler(env) {
  const config = {
    development: 'Bearer F589XkskJbkAAAAAAAAcybairK36OU14TwHJsDLoJpEoVomqAgElvYEW0erQEGQc',
    production: 'Bearer F589XkskJbkAAAAAAAAcybairK36OU14TwHJsDLoJpEoVomqAgElvYEW0erQEGQc'
  };

  return config[env];
}
module.exports = tokenHandler;
