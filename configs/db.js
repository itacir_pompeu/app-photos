// file: configs/db.js - created at 2016-02-27, 08:33
'use strict';

function dbHandler(env) {
  const config = {
    development:  'mongodb://localhost/pompeuapi',
    production: 'mongodb://ItacirPompeu:552525@ds049130.mongolab.com:49130/pompeuapi'
  };

  return config[env];
}
module.exports = dbHandler;
