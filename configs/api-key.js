// file: configs/api-key.js - created at 2016-02-20, 02:56
'use strict';

function apiKeyHandler(env) {
  const config = {
    development : {key : 'secret'},
    production  : {key : 'secret'}
  };

  return config[env];
}
module.exports = apiKeyHandler;
