// file: configs/index.js
'use strict';

const env      = process.env.NODE_ENV || 'development';
exports.token  = require('./token')(env);
exports.apiKey = require('./api-key')(env);
exports.db     = require('./db')(env);
